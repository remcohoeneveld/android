package nl.remcohoeneveld.instavideo;

        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.support.v7.app.ActionBarActivity;
        import android.support.v7.app.AppCompatActivity;
        import android.view.LayoutInflater;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.ProgressBar;
        import android.widget.RatingBar;
        import android.widget.TextView;
        import android.widget.Toast;
        import android.widget.VideoView;

        import com.google.gson.Gson;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.HttpURLConnection;
        import java.net.MalformedURLException;
        import java.net.URL;
        import java.util.ArrayList;
        import java.util.List;

        import nl.remcohoeneveld.instavideo.models.InstagramModel;


public class RetrieveFeedInsta extends AppCompatActivity {

    private VideoView instaVideos;
    private TextView instaText;

    private final String JSON_INSTAVIDEO = "https://www.instagram.com/nfl/media/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feed_instagram);

        instaVideos = (VideoView) findViewById(R.id.);
        instaText = (TextView) findViewById(R.id.);


        new JSON_INSTAVID().execute(JSON_INSTAVIDEO);
    }


    public class JSON_INSTAVID AsyncTask<String, String, List<InstagramModel>>

    {

        @Override
        protected void onPreExecute () {
        super.onPreExecute();
    }

        @Override
        protected List<InstagramModel> doInBackground (String...params){
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            String finalJson = buffer.toString();

            JSONObject parentObject = new JSONObject(finalJson);
            JSONArray parentArray = parentObject.getJSONArray("movies");

            List<InstagramModel> InstagramModelList = new ArrayList<>();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    }
}
