package hoeneveld.remco.memesupreme;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import hoeneveld.remco.memesupreme.models.MemeModel;

public class MainActivity extends AppCompatActivity {

    // get dem memes
    private final String JSON_URL = "https://api.imgflip.com/get_memes";
    // create dem listview
    private ListView Memes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config); // Do it on Application start

        Memes = (ListView) findViewById(R.id.Memes);


        new JSONMEMES().execute(JSON_URL);
    }

    public class JSONMEMES extends AsyncTask<String, String, List<MemeModel>> {

        @Override
        protected List<MemeModel> doInBackground(String... params) {
            HttpURLConnection con = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                con = (HttpURLConnection) url.openConnection();
                con.connect();
                InputStream stream = con.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                String finalJson = buffer.toString();

                JSONObject parentObject = new JSONObject(finalJson);
                JSONObject parentObjectJSONObject = parentObject.getJSONObject("data");
                JSONArray parentArray = parentObjectJSONObject.getJSONArray("memes");
                List<MemeModel> memeModelList = new ArrayList<>();

                Gson gson = new Gson();
                for (int i = 0; i < parentArray.length(); i++) {
                    JSONObject finalObject = parentArray.getJSONObject(i);
                    /**
                     * below single line of code from Gson saves you from writing the json parsing yourself
                     * which is commented below
                     */
                    MemeModel memeModel = gson.fromJson(finalObject.toString(), MemeModel.class); // a single line json parsing using Gson
                    memeModelList.add(memeModel);
                }
                return memeModelList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (con != null) {
                    con.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List<MemeModel> result) {
            super.onPostExecute(result);
            if (result != null) {
                MemeAdapter adapter = new MemeAdapter(getApplicationContext(), R.layout.row, result);
                Memes.setAdapter(adapter);
                Memes.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MemeModel memeModel = result.get(position);
                        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                        intent.putExtra("memeModel", new Gson().toJson(memeModel));
                        startActivity(intent);
                    }
                });
            }
        }

        public class MemeAdapter extends ArrayAdapter {
            private List<MemeModel> memeModelList;
            private int resource;
            private LayoutInflater inflater;

            public MemeAdapter(Context context, int resource, List<MemeModel> objects) {
                super(context, resource, objects);
                memeModelList = objects;
                this.resource = resource;
                inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ViewHolder holder = null;

                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = inflater.inflate(resource, null);
                    holder.MemeImage = (ImageView)convertView.findViewById(R.id.memeImage);
                    holder.MemeName = (TextView)convertView.findViewById(R.id.memeName);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }

                final ViewHolder finalHolder = holder;
                ImageLoader.getInstance().displayImage(memeModelList.get(position).getUrl(), holder.MemeImage, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        finalHolder.MemeImage.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        finalHolder.MemeImage.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        finalHolder.MemeImage.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        finalHolder.MemeImage.setVisibility(View.INVISIBLE);
                    }
                });

                holder.MemeName.setText(memeModelList.get(position).getName());

                return convertView;
            }

            class ViewHolder {
                private ImageView MemeImage;
                private TextView MemeName;
            }

        }


    }
}
